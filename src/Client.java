class Client implements Runnable {
    private final Barber barber;
    private final String name;

    public Client(Barber barber, String name) {
        this.barber = barber;
        this.name = name;
    }
    @Override
    public void run() {
        // спроба зайти в барбершоп
        boolean entered = barber.enterShop(name);
        // якщо впустили - підстригають
        if (entered) {
            try {
                barber.getHaircut(name);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}